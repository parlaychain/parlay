#!/bin/bash
# create multiresolution windows icon
ICON_DST=../../src/qt/res/icons/parlay.ico

convert ../../src/qt/res/icons/parlay-16.png ../../src/qt/res/icons/parlay-32.png ../../src/qt/res/icons/parlay-48.png ${ICON_DST}
